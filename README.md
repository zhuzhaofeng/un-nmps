# 前端监控

> 前端异常监控: 下班时间写着玩玩。 目前添加内存状态,异步请求监控, 静态资源异常,加载的监控, 运行异常监控, 系统异常监控 ; XHR暂时只测试 axios, jQuery Ajax(),原生XMLHttpRequest以及自己封装的异步请求 `fetch`没有测试, 目前只添加了简单的服务端及可视化界面 [文档末有界面截图]

## 环境

- `电脑`
- `node`
- `terminal` | `cmd`
- `编辑器`
- `MongoDB`

---

1. clone 代码到本地
2. 运行 服务端代码 `nmps-server`
> 访问`http://localhost:3000` 即可看到主界面
3. 打开 `nmps-tools` 运行`index.html`
> 这个文件用于 上报异常

---
## 文件夹说明
- `nmps-client`
> vue写的简单页面
- `nmps-client`
> `node`,`koa`写的简单服务端
- `nmps-tools`
> `webpack`写的 异常，静态资源加载信息，内存信息，XHR异常，运行异常等信息捕获工具
> 重点：异常捕获，上报等都在这个文件夹下面 `dist`下面是webpack打包后的文件
> 此文件最佳引入位置是在所有静态资源之前如下[如若在静态资源加载之后 或者 body结束之前部分异常捕获不到]

```html
<!DOCTYPE html>
<html lang="zh-CN-Hans">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>前端监控</title>
    <script type="text/javascript" src="un-watch.min.js"></script>
    <script>
        unWatch.init({
            time: 10000,
            callback: (data) => {
                // 回调函数里面执行异常上报
                new Image().src = 'http://localhost:3000/report?' + JSON.stringify(data);
            }
        });
    </script>
    <!-- 没有此文件 测试上报 -->
    <link rel="stylesheet" href="./style.css">
    <!-- 只是为了测试 添加 S -->
    <link rel="stylesheet" href="https://cdn.bootcss.com/normalize/8.0.1/normalize.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/minireset.css/0.0.2/minireset.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/twitter-bootstrap/4.3.1/css/bootstrap.css">
    <!-- 只是为了测试 添加 E -->
    <link rel="shortcut icon" href="favicon.ico">
</head>

<body>
    <h1>SayHi()</h1>
    <!-- 只是为了测试 添加 S -->
    <script src="https://cdn.bootcss.com/socket.io/2.2.0/socket.io.js"></script>
    <script src="https://cdn.bootcss.com/echarts/4.2.1-rc1/echarts.min.js"></script>
    <script src="https://cdn.bootcss.com/vue/2.6.10/vue.js"></script>
    <script src="https://unpkg.com/axios@0.19.0/dist/axios.min.js"></script>
    <!-- 只是为了测试 添加 E -->
    <!-- 不存在此图片 只是测试异常监听 -->
    <img src="./example.png" alt="">
    <!-- 不存在此文件 只是测试异常监听 -->
    <script src="./example.js"></script>
    <script>
        // 测试
        setTimeout(() => {
            let el = document.createElement('img');
            // 不存在此图片 只是测试异常监听
            el.setAttribute('src', './example.png');
            document.body.append(el);
            // 不存在此接口 只是测试异常监听
            axios.get('http://localhost:3000/test').then(res => {
                console.log(res)
            })
        }, 5000)
        // 测试运行异常上报
        console.log(test)
    </script>
</body>
</html>
```
![001](./exampleImg/001.png)
![002](./exampleImg/002.png)
![003](./exampleImg/003.png)
![004](./exampleImg/004.png)
![005](./exampleImg/005.png)
![006](./exampleImg/006.png)
![007](./exampleImg/007.png)
![008](./exampleImg/008.png)
