import {
    unitConversion,
    compare
} from "../utils";

/**
 * @description 静态资源加载 列表耗时等
 * @return {[type]} [description]
 * @param callback 获取到所有静态资源信息后触发事件
 */
export default (callback) => {
    window.onload = () => {
        // 获取所有静态资源信息
        let entries = performance.getEntries();
        // 定义监听数组
        let resourceWatches = [];
        // 定义一个时间戳 服务端好区分每一次的静态资源
        let batch = new Date().getTime();
        entries.forEach(item => {
            // 资源 加载时间
            let loadTime = item.responseEnd - item.startTime;
            // 资源 名称
            let name = item.name;
            // 资源 大小
            let size = item.decodedBodySize;
            // 判断是否 是 样式
            if (item.entryType === 'resource' && (item.initiatorType === 'link' || item.initiatorType === 'script')) {
                // 判断是否存在 links 项
                resourceWatches.push({
                    batch,
                    name,
                    loadTime,
                    size,
                    type: item.initiatorType,
                    createTime: new Date().getTime()
                })
            }
        })
        let linksTotalLoadTime = 0;
        // 计算所有样式耗时加载时间

        // 需要做 非空处理
        if (resourceWatches.links && resourceWatches.links.length > 0) {
            resourceWatches.links.forEach(item => {
                linksTotalLoadTime += parseFloat(item.loadTime);
            })
            resourceWatches.links.sort(compare('loadTime'), 'desc')
        }
        // 计算所有脚本加载耗时时间
        let scriptsTotalLoadTime = 0;
        if (resourceWatches.scripts && resourceWatches.scripts.length > 0) {
            resourceWatches.scripts.forEach(item => {
                scriptsTotalLoadTime += parseFloat(item.loadTime);
            })
            resourceWatches.scripts.sort(compare('loadTime'), 'desc')
        }
        callback ? callback({
            "type": 'staticInfo',
            "info": {
                "staticList": resourceWatches,
                "cssLoadTime": linksTotalLoadTime.toFixed(2) == 0.00 ? 0 : linksTotalLoadTime.toFixed(2),
                "javascriptLoadTime": scriptsTotalLoadTime.toFixed(2) == 0.00 ? 0 : scriptsTotalLoadTime.toFixed(2),
            }
        }) : null;
    }
}
