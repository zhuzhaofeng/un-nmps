/**
 * @author zhuzhaofeng
 * @description Listener Memory
 * @param time 每隔多久获取一次内存信息
 * @parm callback 获取到内存信息的回调函数
 */
export default (time, callback) => {
    setInterval(() => {
        let size = performance.memory;
        let result = {
            type: 'memory',
            info: {
                usedJSHeapSize: (size.usedJSHeapSize / 1048576).toFixed(2),
                totalJSHeapSize: (size.totalJSHeapSize / 1048576).toFixed(2),
                jsHeapSizeLimit: (size.jsHeapSizeLimit / 1048576).toFixed(2),
                state: size.usedJSHeapSize > size.totalJSHeapSize ? "可能出现内存泄漏" : "正常",
                createTime: new Date().getTime()
            }
        }
        callback(result);
    }, time)
}
