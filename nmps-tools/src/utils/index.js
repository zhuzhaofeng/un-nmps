module.exports = {
    unitConversion(bytes) {
        let size = "";
        if (bytes < 0.1 * 1024) {
            // 小于0.1KB，则转化成B
            size = bytes.toFixed(2) + "B"
        } else if (bytes < 0.1 * 1024 * 1024) {
            // 小于0.1MB，则转化成KB
            size = (bytes / 1024).toFixed(2) + "KB"
        } else if (bytes < 0.1 * 1024 * 1024 * 1024) {
            // 小于0.1GB，则转化成MB
            size = (bytes / (1024 * 1024)).toFixed(2) + "MB"
        } else { // 其他转化成GB
            size = (bytes / (1024 * 1024 * 1024)).toFixed(2) + "GB"
        }
        // 转成字符串
        let sizeStr = size + "";
        // 获取小数点处的索引
        let index = sizeStr.indexOf(".");
        // 获取小数点后两位的值
        let dou = sizeStr.substr(index + 1, 2)
        if (dou == "00") {
            //判断后两位是否为00，如果是则删除00
            return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
        }
        return size;
    },
    compare(propertyName, type = "asc") {
        type = type.toLowerCase(type);
        if (type === 'desc') {
            return (obj1, obj2) => {
                if (obj1[propertyName] > obj2[propertyName]) {
                    return 1;
                } else if (obj1[propertyName] < obj2[propertyName]) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
        if (type === 'asc') {
            return (obj1, obj2) => {
                if (obj1[propertyName] < obj2[propertyName]) {
                    return 1;
                } else if (obj1[propertyName] > obj2[propertyName]) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }
    }
}
