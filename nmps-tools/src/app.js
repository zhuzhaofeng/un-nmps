import listenerMemory from './components/listenerMemory';
import listenerStaticInfo from './components/listenerStaticInfo';
import listenerStaticError from './components/listenerStaticError';
import listenerXHR from './components/listenerXHR';
import listenerError from './components/listenerError';

/**
 * @author zhuzhaofeng
 * @description 初始化方法
 */

const unWatch = {
    config: {},
    init(args = {}) {
        let defaultConfig = {
            time: 3000,
            ignore: ['*'],
            callback: (data) => {
                console.log(data)
            }
        }
        this.config = Object.assign(defaultConfig, args);
        this.reader();
    },
    reader() {
        listenerMemory(this.config.time, this.config.callback)
        listenerXHR(this.config.callback)
        listenerError(this.config.callback)
        listenerStaticInfo(this.config.callback)
        listenerStaticError(this.config.callback)
    }
}
window.unWatch = unWatch;
export default unWatch;
