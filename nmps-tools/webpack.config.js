const path = require('path');
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        index: './src/app.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'un-watch.min.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'public/index.html',
            favicon:'public/favicon.ico',
            inject: 'head'
        }),
        new webpack.HotModuleReplacementPlugin(), //引入热更新插件
        new webpack.BannerPlugin('@author: zhuzhaofeng \n@email: zhuzhaofeng@aliyun.com \n@description: 前端监控'),
    ],
    devServer: {
        host: 'localhost', //服务器的ip地址
        port: 8000, //端口
        open: true, //自动打开页面
        hot: true, //开启热更新
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader",
            options: {
                presets: ["@babel/preset-env"]
            }
        }]
    }
}
