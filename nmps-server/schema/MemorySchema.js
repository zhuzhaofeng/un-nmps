const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const MemorySchema = new Schema({
    _id: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    jsHeapSizeLimit: {
        type: String,
        required: true
    },
    usedJSHeapSize: {
        type: String,
        required: true
    },
    totalJSHeapSize: {
        type: String,
        required: true
    },
    createTime: {
        type: String,
        required: true
    }
})
module.exports = memory = mongoose.model("MemorySchema", MemorySchema);