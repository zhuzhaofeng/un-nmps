const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const EroorSchema = new Schema({
    _id: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    source: {
        type: String,
        required: true
    },
    lineno: {
        type: String,
        required: true
    },
    colno: {
        type: String,
        required: true
    },
    createTime: {
        type: String,
        required: true
    }
})

module.exports = error = mongoose.model("EroorSchema", EroorSchema);