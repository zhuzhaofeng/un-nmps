const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const StaticInfoSchema = new Schema({
    _id: {
        type: String,
        required: true
    },
    batch: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    loadTime: {
        type: String,
        default: ''
    },
    size: {
        type: String,
        default: ''
    },
    createTime: {
        type: String,
        required: true
    }
})

module.exports = staticInfo = mongoose.model("StaticInfoSchema", StaticInfoSchema);