const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const StaticErrorSchema = new Schema({
    _id: {
        type: String,
        required: true
    },
    el: {
        type: String,
        required: true
    },
    msg: {
        type: String,
        required: true
    },
    classes: {
        type: String,
        default: ''
    },
    src: {
        type: String,
        default: ''
    },
    href: {
        type: String,
        default: ''
    },
    html: {
        type: String,
        default: ''
    },
    createTime: {
        type: String,
        required: true
    }
})

module.exports = staticError = mongoose.model("StaticErrorSchema", StaticErrorSchema);