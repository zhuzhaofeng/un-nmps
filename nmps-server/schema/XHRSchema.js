const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const XHRSchema = new Schema({
    _id: {
        type: String,
        required: true
    },
    method: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    async: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    response: {
        type: String,
        required: true
    },
    timeLoad: {
        type: String,
        required: true
    },
    createTime: {
        type: String,
        required: true
    }
})

module.exports = xhr = mongoose.model("XHRSchema", XHRSchema);