module.exports = {
    guid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) =>{
            let r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    getServeDay() {
        let result = [];
        for (let i = -6; i < 1; i++) {
            let dd = new Date();
            dd.setDate(dd.getDate() + i);
            let y = dd.getFullYear();
            //获取当前月份的日期，不足10补0
            let m = (dd.getMonth() + 1) < 10 ? `0${dd.getMonth() + 1}` : dd.getMonth() + 1;
            //获取当前几号，不足10补0
            let d = dd.getDate() < 10 ?  `0${dd.getDate()}` : dd.getDate();
            result.push(`${y}-${m}-${d}`);
        }
        return result;
    },
    timestampToTime(timestamp) {
        function change(t) {
            return t < 10 ? `0${t}` : t;
        }
        let date = new Date(timestamp);
        let year = date.getFullYear();
        let month = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
        let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
        let hours = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
        let minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
        let seconds = date.getSeconds() < 10 ? `0${date.getSeconds()}` : date.getSeconds();
        return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
    },
    PromiseForEach(arr, cb) {
        let realResult = []
        let result = Promise.resolve()
        arr.forEach((a, index) => {
            result = result.then(() => {
                return cb(a).then((res) => {
                    realResult.push(res)
                })
            })
        })
    
        return result.then(() => {
            return realResult
        })
    }
}