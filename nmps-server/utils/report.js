// Mongoose
const mongoose = require('mongoose')

// Schema
const MemorySchema = require('../schema/MemorySchema');
const ErrorSchema = require('../schema/ErrorSchema');
const StaticErrorSchema = require('../schema/StaticErrorSchema');
const StaticInfoSchema = require('../schema/StaticInfoSchema');
const XHRSchema = require('../schema/XHRSchema');

//Utils
const utils = require('./index.js');

module.exports = {
    /**
     * 上报
     * @param {Object} params 
     */
    report(params) {
        return new Promise((resolve, reject) => {
            switch (params.type) {
                case 'staticError':
                    const static = new StaticErrorSchema(
                        Object.assign(params.info, {
                            _id: utils.guid()
                        })
                    );
                    static.save()
                        .then(res => {
                            resolve(res);
                        })
                        .catch(e => {
                            reject(e);
                        })
                    break;
                case 'staticInfo':
                    if(params.info.staticList && params.info.staticList.length > 0){
                        console.log(params.info.staticList)
                        params.info.staticList.forEach(item => {
                            item._id = utils.guid();
                        })
                        StaticInfoSchema.insertMany(params.info.staticList).then(res => {
                            resolve(res);
                        }).catch( err => {
                            console.log(err)
                        })
                    }
                    break;
                case 'error':
                    const err = new ErrorSchema(
                        Object.assign(params.info, {
                            _id: utils.guid()
                        })
                    );
                    err.save()
                        .then(res => {
                            resolve(res);
                        })
                        .catch(e => {
                            reject(e);
                        })
                    break;
                case 'memory':
                    const memory = new MemorySchema(
                        Object.assign(params.info, {
                            _id: utils.guid()
                        })
                    );
                    memory.save()
                        .then(res => {
                            resolve(res);
                        })
                        .catch(e => {
                            reject(e);
                        })
                    break;
                case 'xhr':
                    const xhr = new XHRSchema(
                        Object.assign(params.info, {
                            _id: utils.guid()
                        })
                    );
                    xhr.save()
                        .then(res => {
                            resolve(res);
                        })
                        .catch(e => {
                            reject(e);
                        })
                    break;
            }
        })
    },
    /**
     * 获取数据库数据信息
     * @param {*} days 日期数组 
     * @param {*} type 类型 
     * @paramTemplate ["2019-08-06","2019-08-07","2019-08-08","2019-08-09","2019-08-10"]
     */
    getDataByDays(days, type) {
        let realResult = [];
        let result = Promise.resolve();
        days.forEach((item, i) => {
            let qs = new RegExp(item);
            result = result.then(() => {
                switch (type) {
                    case 'memory':
                        return MemorySchema.find().sort({ createTime: -1 }).limit(10)
                            .then(res => {
                                console.log(createTime);
                            })
                        break;
                    case 'staticError':
                        return StaticErrorSchema.where('createTime', qs)
                            .then(res => {
                                realResult.push({
                                    day: item,
                                    data: res
                                });
                            })
                        break;
                    case 'error':
                        return ErrorSchema.where('createTime', qs)
                            .then(res => {
                                realResult.push({
                                    day: item,
                                    data: res
                                });
                            })
                        break;
                    case 'xhr':
                        return XHRSchema.where('createTime', qs)
                            .then(res => {
                                realResult.push({
                                    day: item,
                                    data: res
                                });
                            })
                        break;
                }
            })
        })
        return result.then(() => {
            return realResult
        })
    }
}