const Koa = require('koa');
const app = new Koa();
const views = require('koa-views')
const json = require('koa-json')
const bodyparser = require('koa-bodyparser')
// Mongoose
const mongoose = require('mongoose')

// middlewares
app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}))
app.use(json())
app.use(require('koa-static')(__dirname + '/public'))
app.use(views(__dirname + '/views', {
    extension: 'html'
}))


// Connection
mongoose.connect('mongodb://localhost/report', { useNewUrlParser: true })
    .then(() => console.log("mongoDB Connection Success!"))
    .catch(e => console.log(e))

const report = require('./routers/report');
const index = require('./routers');
app.use(async (ctx, next) => {
    ctx.set('Access-Control-Allow-Origin', '*');
    await next();
});

// routes
app.use(report.routes(), report.allowedMethods())
app.use(index.routes(), index.allowedMethods())

app.listen(3000);
