/**
 * 路由列表
 * zhuzhaofeng
 */
export default [{
        path: '/',
        name: 'nmpsLayout',
        component: () =>
			import ('@/views/NMPS_Layout'),
			children: [{
				path: '',
				name: 'home',
				component: () =>
					import ('@/views/Home.vue')
			},{
				path: '/exception',
				name: 'exception',
				component: () =>
					import ('@/views/Exception'),
                    children: [{
                        path: '',
                        name: 'exceptionStatic',
                        component: () =>
        					import ('@/views/Exception/Static.vue')
                    },{
                        path: 'static',
                        name: 'exceptionStatic',
                        component: () =>
        					import ('@/views/Exception/Static.vue')
                    },{
                        path: 'error',
                        name: 'exceptionError',
                        component: () =>
        					import ('@/views/Exception/Error.vue')
                    },{
                        path: 'xhr',
                        name: 'exceptionXHR',
                        component:  () =>
                            import ('@/views/Exception/XHR.vue')
                    }]
			},{
				path: '/runtime',
				name: 'runtime',
				component: () =>
					import ('@/views/Runtime')
			},{
				path: '/data',
				name: 'data',
				component: () =>
					import ('@/views/Data')
			}]
    },
    {
        path: '/about',
        name: 'about',
        component: () =>
            import ('@/views/About.vue')
    }
]
