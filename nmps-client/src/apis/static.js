import axios from './axios.js';

export const getNewestStaticInfo = () => {
	return axios.get('/data/static/info')
}

export const getStaticError = () => {
	return axios.get('/data/static/error')
}
