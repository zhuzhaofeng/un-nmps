import axios from './axios.js';

export const getNewestMemory = () => {
	return axios.get('/data/memory')
}
