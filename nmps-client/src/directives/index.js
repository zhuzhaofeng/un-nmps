import Vue from 'vue'
import { markedWords } from '@/lib/tools';
/**
 * Vue自定义指令 设置问候语
 * zhuzhaofeng
 */
 Vue.directive('markedWords', {
     bind(el, binding) {
         el.textContent = markedWords();
     }
 })
 Vue.directive('dynamicColor', {
     bind(el, binding) {
         if(binding.value && binding.value > 0){
             el.classList.add('text-error');
             el.textContent = binding.value;
         } else{
             el.classList.add('text-success');
             el.textContent = 0;
         }
     }
 })
