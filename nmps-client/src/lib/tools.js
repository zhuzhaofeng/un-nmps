/**
 * 工具类 —— 与业务无关的工具方法
 * zhuzhaofeng
 */
module.exports = {
	markedWords() {
		/**
		 * 根据当前时间获取问候语
		 */
		let now = new Date(),
			hour = now.getHours(),
			markedWords = '';
		if (hour < 6) {
			return "凌晨了该休息了哟！";
		} else if (hour > 6 && hour < 9) {
			return "早上好！";
		} else if (hour < 12) {
			return "上午好！";
		} else if (hour < 14) {
			return "中午好！";
		} else if (hour < 17) {
			return "下午好！";
		} else if (hour < 19) {
			return "傍晚好！";
		} else if (hour < 22) {
			return "晚上好！";
		} else {
			return "夜深注意休息哦！";
		}
	},
	unitConversion(bytes) {
		let size = "";
		if (bytes == 0) {
			return 0;
		} else if (bytes < 0.1 * 1024) {
			// 小于0.1KB，则转化成B
			size = bytes.toFixed(2) + "B"
		} else if (bytes < 0.1 * 1024 * 1024) {
			// 小于0.1MB，则转化成KB
			size = (bytes / 1024).toFixed(2) + "KB"
		} else if (bytes < 0.1 * 1024 * 1024 * 1024) {
			// 小于0.1GB，则转化成MB
			size = (bytes / (1024 * 1024)).toFixed(2) + "MB"
		} else { // 其他转化成GB
			size = (bytes / (1024 * 1024 * 1024)).toFixed(2) + "GB"
		}
		// 转成字符串
		let sizeStr = size + "";
		// 获取小数点处的索引
		let index = sizeStr.indexOf(".");
		// 获取小数点后两位的值
		let dou = sizeStr.substr(index + 1, 2)
		if (dou == "00") {
			//判断后两位是否为00，如果是则删除00
			return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
		}
		return size;
	},
	/**
	 * 对象数组排序
	 * @param  {[String]} 需要排序的属性
	 * @param  {String} [type="asc"] 排序类型 asc 升序，desc 降序
	 * @return {[type]}
	 */
	compare(propertyName, type = "asc") {
		type = type.toLowerCase(type);
		if (type === 'desc') {
			return (obj1, obj2) => {
				if (obj1[propertyName] > obj2[propertyName]) {
					return 1;
				} else if (obj1[propertyName] < obj2[propertyName]) {
					return -1;
				} else {
					return 0;
				}
			}
		}
		if (type === 'asc') {
			return (obj1, obj2) => {
				if (obj1[propertyName] < obj2[propertyName]) {
					return 1;
				} else if (obj1[propertyName] > obj2[propertyName]) {
					return -1;
				} else {
					return 0;
				}
			}
		}
	},
	formatDateTime(value) {
		let date = new Date(value);
		let y = date.getFullYear();
		let MM = date.getMonth() + 1;
		MM = MM < 10 ? ('0' + MM) : MM;
		let d = date.getDate();
		d = d < 10 ? ('0' + d) : d;
		let h = date.getHours();
		h = h < 10 ? ('0' + h) : h;
		let m = date.getMinutes();
		m = m < 10 ? ('0' + m) : m;
		let s = date.getSeconds();
		s = s < 10 ? ('0' + s) : s;
		return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s;
	}
}
